<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/error', function() {
    return view('error');
})->name('error');

Route::group(['namespace' => 'User', 'middleware' => 'testmiddleware'], function () {
    Route::get('/user/{name}', 'UserController@show');
});

Route::group(['namespace' => 'Category', 'prefix' => 'product'], function () {
    Route::resource('/ok-test', 'CategoryController');
});


// Assignment 2

Route::resources([
    'first' => 'FirstController',
    'second' => 'SecondController'
]);