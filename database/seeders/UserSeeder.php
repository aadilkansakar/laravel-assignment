<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => Str::random(10),
            'last_name' => Str::random(10),
            'status' => 1,
            'phone_number' => Str::random(10),
            'details' => Str::random(100),
        ]);

        User::create([
            'first_name' => Str::random(5),
            'last_name' => Str::random(5),
            'status' => 0,
            'phone_number' => Str::random(5),
            'details' => Str::random(5),
        ]);
    }
}
