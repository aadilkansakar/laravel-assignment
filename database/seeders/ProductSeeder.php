<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('products')->insert([
        //     'name' => Str::random(10),
        //     'category' => Str::random(10),
        //     'status' => 1,
        // ]);

        // DB::insert('insert into products (name, category, status) values (?, ?, ?)', [Str::random(5), Str::random(5), 0]);

        Product::factory()->count(50)->create();
    }
}
